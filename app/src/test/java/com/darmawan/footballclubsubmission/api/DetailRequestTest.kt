package com.darmawan.footballclubsubmission.api

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.mockito.Mockito

@RunWith(JUnitPlatform::class)
class DetailRequestTest: Spek({

    describe("test api request"){
        val api = Mockito.mock(ApiRepository::class.java)

        on("getPastvent"){
            api.getEventDetail("441613")

            it("request success"){
                Mockito.verify(api).getEventDetail("441613")
            }
        }

    }



})