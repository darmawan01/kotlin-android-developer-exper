package com.darmawan.footballclubsubmission.views.team.detailteam.fragment.player

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.adapter.AdapterPlayer
import com.darmawan.footballclubsubmission.models.player.Player
import com.darmawan.footballclubsubmission.models.team.Team
import com.darmawan.footballclubsubmission.utils.invisible
import com.darmawan.footballclubsubmission.utils.visible
import com.darmawan.footballclubsubmission.views.team.detailteam.fragment.player.detail.DetailPlayer
import kotlinx.android.synthetic.main.fragment_player_team.*
import org.jetbrains.anko.support.v4.startActivity

class PlayerFragment: Fragment(), PlayerView{

    private val players: MutableList<Player> = ArrayList()
    private lateinit var presenter: PlayerPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_player_team, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val team = arguments?.getParcelable<Team>("TEAMS")

        presenter = PlayerPresenter(this)
        presenter.getPlayers(team?.idTeam)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = AdapterPlayer(players){
            startActivity<DetailPlayer>(
                    "strPlayer" to "${it.strPlayer}",
                    "dateBorn" to "${it.dateBorn}",
                    "strHeight" to "${it.strHeight}",
                    "strWeight" to "${it.strWeight}",
                    "strFanart1" to "${it.strFanart1}",
                    "strDescriptionEN" to "${it.strDescriptionEN}",
                    "strThumb" to "${it.strThumb}"
            )
        }

        rc_player_team.layoutManager = LinearLayoutManager(this.context)
        rc_player_team.adapter = adapter

    }

    override fun setPlayer(player: MutableList<Player>) {
        players.clear()
        players.addAll(player)
    }

    override fun showLoading() {
        player_progress.visible()
    }

    override fun hideLoading() {
        player_progress.invisible()
    }


}