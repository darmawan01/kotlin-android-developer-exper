package com.darmawan.footballclubsubmission.views.team.detailteam.fragment.player

import com.darmawan.footballclubsubmission.models.player.Player

interface PlayerView{
    fun showLoading()
    fun hideLoading()
    fun setPlayer(player: MutableList<Player>)
}