package com.darmawan.footballclubsubmission.views.match.detailmatch

import com.darmawan.footballclubsubmission.models.brand.Brand
import com.darmawan.footballclubsubmission.models.event.Event

interface DetailMatchView{
    fun showLoading()
    fun hideLoading()
    fun setData(events: List<Event>)
    fun setImgHome(homeBrandUrl: List<Brand>?)
    fun setImgAway(awayBrandUrl: List<Brand>)
}