package com.darmawan.footballclubsubmission.views.favorite.team

import android.content.Intent
import android.support.v4.app.Fragment
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.adapter.AdapterTeam
import com.darmawan.footballclubsubmission.models.team.Team
import com.darmawan.footballclubsubmission.views.team.detailteam.DetailTeamActivity
import kotlinx.android.synthetic.main.fragment_team_fav.*

class TeamFavFragment: Fragment(), SwipeRefreshLayout.OnRefreshListener{

    private lateinit var presenter: TeamFavPresenter
    private val teams: MutableList<Team> = ArrayList()
    private lateinit var adapterTeam: AdapterTeam

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_fav, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = TeamFavPresenter(this.context)
        presenter.showFavMatch(teams)

        adapterTeam = AdapterTeam(teams){
            val bundle = Bundle()
            bundle.putParcelable("TEAMS", it)

            startActivity(
                    Intent(this.context, DetailTeamActivity::class.java).putExtras(bundle)
            )
        }

        rc_favteam.layoutManager = LinearLayoutManager(this.context)
        rc_favteam.adapter = adapterTeam

        favteam_swipe.setOnRefreshListener(this)
    }

    override fun onRefresh() {
        if (favteam_swipe.isRefreshing){
            Handler().postDelayed(Runnable{
                teams.clear()
                presenter.showFavMatch(teams)
                adapterTeam.notifyDataSetChanged()
                favteam_swipe.isRefreshing = false
            }, 500)
        }
    }

}