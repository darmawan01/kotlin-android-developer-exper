package com.darmawan.footballclubsubmission.views.favorite.event

interface EventFavView{
    fun showLoading()
    fun hideLoading()
}