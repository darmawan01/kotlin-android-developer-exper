package com.darmawan.footballclubsubmission.views.favorite

import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.utils.ViewPagerAdapter
import com.darmawan.footballclubsubmission.views.favorite.event.EventFavFragment
import com.darmawan.footballclubsubmission.views.favorite.team.TeamFavFragment
import kotlinx.android.synthetic.main.fragment_base_fav.*

class BaseFavFragment: Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_base_fav, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setFragment()
    }

    private fun setFragment(){
        val viewAdapter = ViewPagerAdapter(childFragmentManager)
        viewAdapter.addFragment(EventFavFragment(), "Event Favorite")

        viewAdapter.addFragment(TeamFavFragment(), "Team Favorite")

        base_fav_viewpager.adapter = viewAdapter

        base_fav_tab.setupWithViewPager(base_fav_viewpager)
    }

}