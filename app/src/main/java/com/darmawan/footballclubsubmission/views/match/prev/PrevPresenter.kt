package com.darmawan.footballclubsubmission.views.match.prev

import android.util.Log
import com.darmawan.footballclubsubmission.api.ApiService
import com.darmawan.footballclubsubmission.api.ApiRepository
import com.darmawan.footballclubsubmission.models.event.Event
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PrevPresenter(private val view: PrevView) {
    private val service = ApiService.retrofit.create(ApiRepository::class.java)
    fun getPrevEvent(idLeague: String?) {

        view.showLoading()

        async(UI) {
            val event = bg {
                service.getPastEvent(idLeague).execute()
            }

            event.await().body()?.events?.let { view.setEventList(it) }
            view.hideLoading()

        }
    }

    fun getLeague(){
        view.showLoading()
        async(UI) {
            val leagues = bg {
                service.getLeague().execute()
            }

            leagues.await().body()?.leagues?.let {
                this@PrevPresenter.view.spinerEntry(it)
            }
            view.hideLoading()
        }
    }
}