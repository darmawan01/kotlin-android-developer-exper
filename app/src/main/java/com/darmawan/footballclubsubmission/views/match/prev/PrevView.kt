package com.darmawan.footballclubsubmission.views.match.prev

import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.models.league.League

interface PrevView {
    fun showLoading()
    fun hideLoading()
    fun setEventList(event: MutableList<Event>)
    fun spinerEntry(league: MutableList<League>)
}