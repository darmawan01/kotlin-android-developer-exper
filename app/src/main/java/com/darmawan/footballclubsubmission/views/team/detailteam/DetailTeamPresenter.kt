package com.darmawan.footballclubsubmission.views.team.detailteam

import android.content.Context
import android.widget.Toast
import com.darmawan.footballclubsubmission.models.team.Team
import com.darmawan.footballclubsubmission.models.teamsqlite.TeamSqlite
import com.darmawan.footballclubsubmission.utils.database
import org.jetbrains.anko.db.*
import java.util.ArrayList

class DetailTeamPresenter(private val context: Context){

    fun getFavMatch(idTeam: String?): MutableList<TeamSqlite>{

        val teams: MutableList<TeamSqlite> = ArrayList()

        context.database.use {
            select("FavTeam")
                    .whereArgs("(idTeam = {idTeam})",
                            "idTeam" to "${idTeam}").exec {
                        parseList(object : MapRowParser<MutableList<TeamSqlite>> {
                            override fun parseRow(columns: Map<String, Any?>): MutableList<TeamSqlite> {

                                val team = TeamSqlite(
                                        columns["idTeam"].toString()
                                )

                                teams.add(team)

                                return teams
                            }

                        })
                    }

        }
        return teams
    }

    fun addFavorite(team: Team) {
        context.database.use {
            insert(
                    "FavTeam",
                    "idTeam" to team.idTeam,
                    "strTeam" to team.strTeam,
                    "strDescriptionEN" to team.strDescriptionEN,
                    "strTeamBadge" to team.strTeamBadge
            )
        }
        Toast.makeText(context, "Favorite Added !", Toast.LENGTH_SHORT).show()
    }

    fun rmFavorite(idTeam: String?) {
        this.context.database.use {
            delete("FavTeam", "idTeam = {id_team}", "id_team" to "${idTeam}")
            Toast.makeText(context, "Favorite Removed !", Toast.LENGTH_SHORT).show()
        }
    }
}