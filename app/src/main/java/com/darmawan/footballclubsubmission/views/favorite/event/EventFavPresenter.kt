package com.darmawan.footballclubsubmission.views.favorite.event

import android.content.Context
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.utils.database
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.select
import java.util.*

class EventFavPresenter(private val view: EventFavView, private val context: Context?){

    fun showFavMatch(events: MutableList<Event>){
        view.showLoading()
        this.context?.database?.use{
            select("FavMatch").exec {
                parseList(object :MapRowParser<MutableList<Event>>{
                    override fun parseRow(columns: Map<String, Any?>): MutableList<Event> {
                        val data = Event(
                                idEvent = columns["idEvent"].toString(),
                                idSoccerXML = columns["idSoccerXML"].toString(),
                                idHomeTeam = columns["idHomeTeam"].toString(),
                                strHomeTeam = columns["strHomeTeam"].toString(),
                                idAwayTeam = columns["idAwayTeam"].toString(),
                                strAwayTeam = columns["strAwayTeam"].toString(),
                                intHomeScore = if (columns["intHomeScore"].toString() != "null") columns["intHomeScore"].toString() else "",
                                intAwayScore = if (columns["intAwayScore"].toString() != "null") columns["intAwayScore"].toString() else "",
                                dateEvent = Date(columns["dateEvent"].toString()),
                                strDate = columns["strDate"].toString()
                        )
                        events.add(data)

                        return events
                    }

                })
            }
        }
        view.hideLoading()

    }
}