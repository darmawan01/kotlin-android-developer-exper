package com.darmawan.footballclubsubmission.views.team.detailteam.fragment.player.detail

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.darmawan.footballclubsubmission.R
import kotlinx.android.synthetic.main.activity_detail_player.*

class DetailPlayer : AppCompatActivity() {

    private lateinit var playerName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)

        val intent = intent
        setDate(intent)

        setSupportActionBar(player_dt_toolbar)
        val ab = supportActionBar
        ab?.setDisplayHomeAsUpEnabled(true)
        ab?.title = playerName

        player_dt_toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    private fun setDate(intent: Intent){
        val strPlayer = intent.getStringExtra("strPlayer")
        val strHeight = intent.getStringExtra("strHeight")
        val strWeight = intent.getStringExtra("strWeight")
        val strFanart1 = intent.getStringExtra("strFanart1")
        val strDescriptionEN = intent.getStringExtra("strDescriptionEN")

        playerName = strPlayer
        txt_dtl_player_weight.text = strWeight
        txt_dtl_player_height.text = strHeight
        txt_player_forward.text = strDescriptionEN
        Glide.with(this).load(strFanart1).into(txt_dtl_player_fanart)


    }
}
