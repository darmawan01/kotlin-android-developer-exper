package com.darmawan.footballclubsubmission.views.team.team

import com.darmawan.footballclubsubmission.models.league.League
import com.darmawan.footballclubsubmission.models.team.Team

interface TeamView{
    fun showLoading()
    fun hideLoading()
    fun setTeamList(teams: MutableList<Team>)
    fun spinerEntry(league: MutableList<League>)
}