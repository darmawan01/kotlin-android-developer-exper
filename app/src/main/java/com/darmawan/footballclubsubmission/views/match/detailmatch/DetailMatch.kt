package com.darmawan.footballclubsubmission.views.match.detailmatch

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.bumptech.glide.Glide
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.models.brand.Brand
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.utils.dateParsers
import com.darmawan.footballclubsubmission.utils.invisible
import com.darmawan.footballclubsubmission.utils.visible
import kotlinx.android.synthetic.main.activity_detail.*

class DetailMatch : AppCompatActivity(), DetailMatchView {

    private var list: MutableList<Event> = mutableListOf()
    private lateinit var presenter: DetailMatchPresenter
    private lateinit var view: DetailMatchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val intent = intent
        val idEvent: String? = intent.getStringExtra("idEvents")
        val idHomeTeam: String? = intent.getStringExtra("idHometeam")
        val idAwayTeam: String? = intent.getStringExtra("idAwayteam")

        presenter = DetailMatchPresenter(this, this)
        presenter.getDetailEvent(idEvent)

        presenter.getTeamHomeBrands(idHomeTeam)
        presenter.getTeamAwayBrands(idAwayTeam)

        toolbar.setOnClickListener {
            this.onBackPressed()
        }

        if (isFavorite(idEvent)){
            btn_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_star_yellow_black_24dp))
        }

        btn_favorite.setOnClickListener {

            if (isFavorite(idEvent)){
                presenter.rmFavorite(idEvent)
                btn_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_star_black_24dp))
            }else{
                presenter.addFavorite(list)
                btn_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_star_yellow_black_24dp))
            }

        }
    }


    override fun setData(events: List<Event>) {
        txt_dtl_date_event.text = dateParsers(events[0].dateEvent)
        txt_dtl_home_score.text = events[0].intHomeScore
        txt_dtl_away_score.text = events[0].intAwayScore
        txt_dtl_home_team.text = events[0].strHomeTeam
        txt_dtl_away_team.text = events[0].strAwayTeam
        txt_dtl_home_goal_detail.text = events[0].strHomeGoalDetails
        txt_dtl_away_goal_detail.text = events[0].strAwayGoalDetails
        txt_dtl_home_shots.text = events[0].intHomeShots
        txt_dtl_away_shot.text = events[0].intAwayShots
        txt_dtl_home_goal_keeper.text = events[0].strHomeLineupGoalkeeper
        txt_dtl_away_goal_leeper.text = events[0].strAwayLineupGoalkeeper
        txt_dtl_home_defense.text = events[0].strHomeLineupDefense
        txt_dtl_away_defense.text = events[0].strAwayLineupDefense
        txt_dtl_home_midfield.text = events[0].strHomeLineupMidfield
        txt_dtl_away_midfield.text = events[0].strAwayLineupMidfield
        txt_dtl_home_forward.text = events[0].strHomeLineupForward
        txt_dtl_away_forward.text = events[0].strAwayLineupForward
        txt_dtl_home_substitutes.text = events[0].strHomeLineupSubstitutes
        txt_dtl_away_substitutes.text = events[0].strAwayLineupSubstitutes

        list.addAll(events)
    }

    override fun setImgHome(homeBrandUrl: List<Brand>?) {
        Glide.with(this).load(homeBrandUrl?.get(0)?.strTeamBadge).into(txt_dtl_home_team_logo)
    }

    override fun setImgAway(awayBrandUrl: List<Brand>) {
        Glide.with(this).load(awayBrandUrl[0].strTeamBadge).into(txt_dtl_away_team_logo)
    }

    override fun showLoading() {
        detail_progress.visible()
    }

    override fun hideLoading() {
        detail_progress.invisible()
    }

    private fun isFavorite(idEvent: String?): Boolean {
        val item =  presenter.getFavMatch(idEvent)

        if (item.size > 0 ){
            return true
        }

        return false
    }

}
