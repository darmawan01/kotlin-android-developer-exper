package com.darmawan.footballclubsubmission.views.favorite.team

import android.content.Context
import com.darmawan.footballclubsubmission.models.team.Team
import com.darmawan.footballclubsubmission.utils.database
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.select

class TeamFavPresenter(private val context: Context?){

    fun showFavMatch(teams: MutableList<Team>){
        this.context?.database?.use{
            select("FavTeam").exec {
                parseList(object : MapRowParser<MutableList<Team>> {
                    override fun parseRow(columns: Map<String, Any?>): MutableList<Team> {
                        val data = Team(
                                idTeam = columns["idTeam"].toString(),
                                strTeam = columns["strTeam"].toString(),
                                strDescriptionEN = columns["strDescriptionEN"].toString(),
                                strTeamBadge = columns["strTeamBadge"].toString()
                        )
                        teams.add(data)

                        return teams
                    }

                })
            }
        }

    }
}