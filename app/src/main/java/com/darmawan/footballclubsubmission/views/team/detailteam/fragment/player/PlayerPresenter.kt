package com.darmawan.footballclubsubmission.views.team.detailteam.fragment.player

import android.util.Log
import com.darmawan.footballclubsubmission.api.ApiRepository
import com.darmawan.footballclubsubmission.api.ApiService
import com.darmawan.footballclubsubmission.models.player.PlayerResponse
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlayerPresenter(private val view: PlayerView){
    fun getPlayers(idTeam: String?){
        view.showLoading()

        async(UI) {
            val service = ApiService.retrofit.create(ApiRepository::class.java)
            val player = bg {
                service.getPlayer(idTeam).execute()
            }

            player.await().body()?.player?.let { view.setPlayer(it) }
            view.hideLoading()
        }

    }
}