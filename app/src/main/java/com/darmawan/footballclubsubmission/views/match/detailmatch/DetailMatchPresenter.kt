package com.darmawan.footballclubsubmission.views.match.detailmatch

import android.content.Context
import android.widget.Toast
import com.darmawan.footballclubsubmission.api.ApiService
import com.darmawan.footballclubsubmission.api.ApiRepository
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.models.eventsqlite.EventSqlite
import com.darmawan.footballclubsubmission.utils.database
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.db.*
import java.util.*

class DetailMatchPresenter(private val view: DetailMatchView, private val context: Context){

    fun getDetailEvent(eventId: String?){
        view.showLoading()

        async(UI) {
            val service = ApiService.retrofit.create(ApiRepository::class.java)
            val event = bg {
                service.getEventDetail(eventId).execute()
            }

            event.await().body()?.events?.let { view.setData(it) }
            view.hideLoading()
        }

    }

    fun getTeamHomeBrands(teamHomeId: String?){

        async(UI) {
            val service = ApiService.retrofit.create(ApiRepository::class.java)
            val event = bg {
                service.getTeamBrand(teamHomeId).execute()
            }

            view.setImgHome(event.await().body()?.teams)
            view.hideLoading()
        }
    }

    fun getTeamAwayBrands(teamAwayId: String?){

        async(UI) {
            val service = ApiService.retrofit.create(ApiRepository::class.java)
            val event = bg {
                service.getTeamBrand(teamAwayId).execute()
            }

            event.await().body()?.teams?.let { view.setImgAway(it) }
            view.hideLoading()
        }
    }

    fun getFavMatch(idEvent: String?): MutableList<EventSqlite>{

        val events: MutableList<EventSqlite> = ArrayList()

        context.database.use {
            select("FavMatch")
                    .whereArgs("(idEvent = {idEvent})",
                            "idEvent" to "${idEvent}").exec {
                        parseList(object : MapRowParser<MutableList<EventSqlite>> {
                            override fun parseRow(columns: Map<String, Any?>): MutableList<EventSqlite> {

                                val event = EventSqlite(
                                        columns["idEvent"].toString()
                                )

                                events.add(event)

                                return events
                            }

                        })
                    }

        }
        return events
    }

    fun addFavorite(event: MutableList<Event>) {
        context.database.use {
            insert(
                    "FavMatch",
                    "idEvent" to event[0].idEvent,
                    "idSoccerXML" to event[0].idSoccerXML,
                    "idHomeTeam" to event[0].idHomeTeam,
                    "idAwayTeam" to event[0].idAwayTeam,
                    "strHomeTeam" to event[0].strHomeTeam,
                    "strAwayTeam" to event[0].strAwayTeam,
                    "intHomeScore" to event[0].intHomeScore,
                    "intAwayScore" to event[0].intAwayScore,
                    "dateEvent" to event[0].dateEvent.toString(),
                    "strDate" to event[0].strDate
            )
        }
        Toast.makeText(context, "Favorite Added !", Toast.LENGTH_SHORT).show()
    }

    fun rmFavorite(idEvent: String?) {
        this.context.database.use {
            delete("FavMatch", "idEvent = {id_event}", "id_event" to "${idEvent}")
            Toast.makeText(context, "Favorite Removed !", Toast.LENGTH_SHORT).show()
        }
    }

}