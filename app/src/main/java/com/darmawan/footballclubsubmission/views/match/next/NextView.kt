package com.darmawan.footballclubsubmission.views.match.next

import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.models.league.League

interface NextView{
    fun showLoading()
    fun hideLoading()
    fun setEventList(event: MutableList<Event>)
    fun spinerEntry(league: MutableList<League>)
}