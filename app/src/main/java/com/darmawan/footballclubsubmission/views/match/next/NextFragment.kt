package com.darmawan.footballclubsubmission.views.match.next

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.adapter.AdapterMatch
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.models.league.League
import com.darmawan.footballclubsubmission.utils.invisible
import com.darmawan.footballclubsubmission.utils.visible
import com.darmawan.footballclubsubmission.views.match.detailmatch.DetailMatch
import kotlinx.android.synthetic.main.fragment_next_match.*
import kotlinx.android.synthetic.main.fragment_prev_match.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.support.v4.startActivity
import android.support.v7.widget.SearchView.OnQueryTextListener

class NextFragment: Fragment(), NextView, AdapterView.OnItemSelectedListener, OnQueryTextListener {

    private var events = mutableListOf<Event>()
    private var leagues = mutableListOf<League>()
    private lateinit var presenter: NextPresenter
    private var adapterMatch: AdapterMatch? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_next_match, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = NextPresenter(this)
        presenter.getLeague()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun spinerEntry(league: MutableList<League>) {
        leagues.addAll(league)

        val spinAdapter = ArrayAdapter<League>(
                activity,
                android.R.layout.simple_spinner_item,
                league
        )

        next_spinner.adapter = spinAdapter
        next_spinner.onItemSelectedListener = this

        val leagueId = league[0].idLeague
        presenter.getNextEvent(leagueId)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        with(leagues[position]){
            idLeague?.let { presenter.getNextEvent(it) }
        }
    }

    override fun setEventList(event: MutableList<Event>) {
        event.let {
            this.events.clear()
            this.events.addAll(it)
            adapterMatch = AdapterMatch(it) {
                startActivity<DetailMatch>(
                        "idEvents" to it.idEvent,
                        "idHometeam" to "${it.idHomeTeam}",
                        "idAwayteam" to "${it.idAwayTeam}"
                )
            }
        }

        rc_next.layoutManager = LinearLayoutManager(this.context)
        rc_next.adapter = adapterMatch
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.search_bar, menu)
        val item = menu?.findItem(R.id.search_list)
        val searchView: SearchView = MenuItemCompat.getActionView(item) as SearchView
        searchView.setOnQueryTextListener(this)

        MenuItemCompat.setOnActionExpandListener(item, object : MenuItemCompat.OnActionExpandListener{
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                adapterMatch?.setFilterList(events)
                return true
            }

        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.search_list -> {
                context?.longToast("Search Event")

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val event = getFilter(events, newText)
        this.adapterMatch?.setFilterList(event)

        return true
    }

    private fun getFilter(events: MutableList<Event>, query: String?): MutableList<Event> {
        val querys = query?.toLowerCase()
        val listFiltered = mutableListOf<Event>()
        for (event in events) {
            val text = "${event.strEvent?.toLowerCase()}"
            querys?.let {
                if (text.contains(it)) {
                    listFiltered.add(event)
                }
            }
        }

        return listFiltered
    }

    override fun showLoading() {
        next_progress.visible()
    }

    override fun hideLoading() {
        next_progress.invisible()
    }
}
