package com.darmawan.footballclubsubmission.views.match.next

import com.darmawan.footballclubsubmission.api.ApiService
import com.darmawan.footballclubsubmission.api.ApiRepository
import com.darmawan.footballclubsubmission.models.event.Event
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class NextPresenter(private val view: NextView){

    private val service = ApiService.retrofit.create(ApiRepository::class.java)
    fun getNextEvent(idLeague: String?){

        view.showLoading()

        async(UI) {
            val event = bg {
                service.getNextEvent(idLeague).execute()
            }

            event.await().body()?.events?.let { view.setEventList(it) }
            view.hideLoading()
        }

    }

    fun getLeague(){
        async(UI) {
            view.showLoading()
            val leagues = bg {
                service.getLeague().execute()
            }

            leagues.await().body()?.leagues?.let { this@NextPresenter.view.spinerEntry(it) }
            view.hideLoading()
        }
    }
}