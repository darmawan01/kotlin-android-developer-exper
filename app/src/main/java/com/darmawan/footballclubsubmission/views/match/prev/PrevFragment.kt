package com.darmawan.footballclubsubmission.views.match.prev

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.support.v7.widget.SearchView
import android.util.Log
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.adapter.AdapterMatch
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.models.league.League
import com.darmawan.footballclubsubmission.utils.invisible
import com.darmawan.footballclubsubmission.utils.visible
import com.darmawan.footballclubsubmission.views.match.detailmatch.DetailMatch
import kotlinx.android.synthetic.main.fragment_prev_match.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.support.v4.startActivity
import android.support.v7.widget.SearchView.OnQueryTextListener
import org.jetbrains.anko.appcompat.v7.coroutines.onClose
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.toast

class PrevFragment : Fragment(), PrevView, AdapterView.OnItemSelectedListener, OnQueryTextListener {

    private var events = mutableListOf<Event>()
    private var leagues = mutableListOf<League>()

    private lateinit var presenter: PrevPresenter
    private var adapterMatch: AdapterMatch? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_prev_match, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = PrevPresenter(this)
        presenter.getLeague()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun spinerEntry(league: MutableList<League>) {
        leagues.addAll(league)
        val spinAdapter = ArrayAdapter<League>(
                activity,
                android.R.layout.simple_spinner_item,
                league
        )

        prev_league_spinner.adapter = spinAdapter
        prev_league_spinner.onItemSelectedListener = this

        val leagueId = league[0].idLeague
        presenter.getPrevEvent(leagueId)
    }

    override fun setEventList(event: MutableList<Event>) {
        event.let {
            this.events.clear()
            this.events.addAll(it)
            adapterMatch = AdapterMatch(it) {
                startActivity<DetailMatch>(
                        "idEvents" to "${it.idEvent}",
                        "idHometeam" to "${it.idHomeTeam}",
                        "idAwayteam" to "${it.idAwayTeam}"
                )
            }
        }

        rc_prev.layoutManager = LinearLayoutManager(this.context)
        rc_prev.adapter = adapterMatch
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        with(this.leagues[position]) {
            idLeague?.let { presenter.getPrevEvent(it) }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.search_bar, menu)
        val item = menu?.findItem(R.id.search_list)
        val searchView: SearchView = MenuItemCompat.getActionView(item) as SearchView
        searchView.setOnQueryTextListener(this)

        MenuItemCompat.setOnActionExpandListener(item, object : MenuItemCompat.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                adapterMatch?.setFilterList(events)
                return true
            }

        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.search_list -> {
                context?.toast("Filter Event")

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val event = getFilter(events, newText)
        this.adapterMatch?.setFilterList(event)

        return true
    }

    private fun getFilter(eventz: MutableList<Event>, query: String?): MutableList<Event> {
        val querys = query?.toLowerCase()

        val listFiltered = mutableListOf<Event>()
        for (event in eventz) {
            val text = "${event.strEvent?.toLowerCase()}"
            querys?.let {
                if (text.contains(it)) {
                    listFiltered.add(event)
                }
            }
        }
        return listFiltered
    }

    override fun showLoading() {
        prev_progress.visible()
    }

    override fun hideLoading() {
        prev_progress.invisible()
    }
}