package com.darmawan.footballclubsubmission.views.team.detailteam.fragment.overview

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.models.team.Team
import kotlinx.android.synthetic.main.fragment_overview_team.*

class OverViewFragment: Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_overview_team, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val team = arguments?.getParcelable<Team>("TEAMS")

        txt_overview.text = team?.strDescriptionEN.toString()

    }

}