package com.darmawan.footballclubsubmission.views.favorite.event

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.adapter.AdapterMatch
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.utils.invisible
import com.darmawan.footballclubsubmission.utils.visible
import com.darmawan.footballclubsubmission.views.match.detailmatch.DetailMatch
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.jetbrains.anko.support.v4.startActivity

class EventFavFragment: Fragment(), EventFavView, SwipeRefreshLayout.OnRefreshListener{

    private lateinit var eventFavPresenter: EventFavPresenter
    private var events: MutableList<Event> = ArrayList()
    private var adapterMatch: AdapterMatch? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        eventFavPresenter = EventFavPresenter(this, this.context)
        eventFavPresenter.showFavMatch(events)

        adapterMatch = AdapterMatch(events){
            startActivity<DetailMatch>(
                    "idEvents" to "${it.idEvent}",
                    "idHometeam" to "${it.idHomeTeam}",
                    "idAwayteam" to "${it.idAwayTeam}"
            )
        }

        rc_favorite.layoutManager = LinearLayoutManager(this.context)
        rc_favorite.adapter = adapterMatch
        swipe.setOnRefreshListener(this)
    }


    override fun onRefresh() {
        if (swipe.isRefreshing){
            Handler().postDelayed(Runnable{
                events.clear()
                eventFavPresenter.showFavMatch(events)
                adapterMatch?.notifyDataSetChanged()
                swipe.isRefreshing = false
            }, 500)
        }
    }

    override fun showLoading() {
        favorite_progress.visible()
    }

    override fun hideLoading() {
        favorite_progress.invisible()
    }
}