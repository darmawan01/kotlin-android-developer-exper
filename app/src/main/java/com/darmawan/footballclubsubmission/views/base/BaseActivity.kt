package com.darmawan.footballclubsubmission.views.base

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.utils.disableShiftMode
import com.darmawan.footballclubsubmission.views.favorite.BaseFavFragment
import com.darmawan.footballclubsubmission.views.match.next.NextFragment
import com.darmawan.footballclubsubmission.views.match.prev.PrevFragment
import com.darmawan.footballclubsubmission.views.team.team.TeamFragment
import kotlinx.android.synthetic.main.activity_base.*
import org.jetbrains.anko.find

class BaseActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val fragment: Fragment?

        when (item.itemId) {
            R.id.navigation_prev_match -> {
                fragment = PrevFragment()
                openFragment(fragment)

            }
            R.id.navigation_next_match -> {
                fragment = NextFragment()
                openFragment(fragment)

            }
            R.id.navigation_team -> {
                fragment = TeamFragment()
                openFragment(fragment)
            }
            R.id.navigation_favorite -> {
                fragment = BaseFavFragment()
                openFragment(fragment)
            }
            else -> {
                return@OnNavigationItemSelectedListener false
            }
        }

    }

    private fun openFragment(fragment: Fragment): Boolean {

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        navigation.disableShiftMode()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val prevMatchFragment = PrevFragment()
        openFragment(prevMatchFragment)

    }
}
