package com.darmawan.footballclubsubmission.views.team.team

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.adapter.AdapterTeam
import com.darmawan.footballclubsubmission.models.league.League
import com.darmawan.footballclubsubmission.models.team.Team
import com.darmawan.footballclubsubmission.utils.invisible
import com.darmawan.footballclubsubmission.utils.visible
import com.darmawan.footballclubsubmission.views.team.detailteam.DetailTeamActivity
import kotlinx.android.synthetic.main.fragment_prev_match.*
import kotlinx.android.synthetic.main.fragment_team.*
import java.util.ArrayList
import android.support.v7.widget.SearchView.OnQueryTextListener
import android.view.*
import com.darmawan.footballclubsubmission.models.event.Event
import org.jetbrains.anko.longToast

class TeamFragment: Fragment(), TeamView, AdapterView.OnItemSelectedListener, OnQueryTextListener {

    private val team = mutableListOf<Team>()
    private val leagues = mutableListOf<League>()

    private lateinit var presenter: TeamPresenter
    private var adapterTeam: AdapterTeam? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = TeamPresenter(this)
        presenter.getLeague()

        setHasOptionsMenu(true)

    }

    override fun spinerEntry(league: MutableList<League>) {
        leagues.addAll(league)

        val spinAdapter = ArrayAdapter<League>(
                activity,
                android.R.layout.simple_spinner_item,
                league
        )

        team_spinner.adapter = spinAdapter
        team_spinner.onItemSelectedListener = this
    }

    override fun setTeamList(teams: MutableList<Team>) {
        teams.let {
            this.team.clear()
            this.team.addAll(it)
            adapterTeam = AdapterTeam(it){
                val bundle = Bundle()
                bundle.putParcelable("TEAMS", it)

                startActivity(
                        Intent(this.context, DetailTeamActivity::class.java).putExtras(bundle)
                )
            }

            rc_team.layoutManager = LinearLayoutManager(this.context)
            rc_team.adapter = adapterTeam
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        with(leagues[position]){
            idLeague?.let { presenter.getTeam(it) }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.search_bar, menu)
        val item = menu?.findItem(R.id.search_list)
        val searchView: SearchView = MenuItemCompat.getActionView(item) as SearchView
        searchView.setOnQueryTextListener(this)

        MenuItemCompat.setOnActionExpandListener(item, object : MenuItemCompat.OnActionExpandListener{
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                adapterTeam?.setFilterList(team)
                return true
            }

        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.search_list -> {
                context?.longToast("Search Team")

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val teams = getFilter(team, newText)
        this.adapterTeam?.setFilterList(teams)

        return true
    }

    private fun getFilter(teams: MutableList<Team>, query: String?): MutableList<Team> {
        val querys = query?.toLowerCase()
        val listFiltered = mutableListOf<Team>()
        for (team in teams) {
            val text = "${team.strTeam?.toLowerCase()}"

            querys?.let {
                if (text.contains(it)) {
                    listFiltered.add(team)
                }
            }
        }

        return listFiltered
    }

    override fun showLoading() {
        team_progress.visible()
    }

    override fun hideLoading() {
        team_progress.invisible()
    }

}
