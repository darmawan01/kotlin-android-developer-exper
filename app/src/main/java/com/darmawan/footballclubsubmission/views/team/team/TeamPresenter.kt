package com.darmawan.footballclubsubmission.views.team.team

import com.darmawan.footballclubsubmission.api.ApiRepository
import com.darmawan.footballclubsubmission.api.ApiService
import com.darmawan.footballclubsubmission.models.team.Team
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class TeamPresenter(private val view: TeamView){
    private val service = ApiService.retrofit.create(ApiRepository::class.java)
    fun getTeam(idLeague: String){
        view.showLoading()

        async(UI){
            val team = bg {
                service.getTeam(idLeague).execute()
            }

            team.await().body()?.teams?.let { view.setTeamList(it) }
            view.hideLoading()
        }
    }

    fun getLeague(){
        view.showLoading()
        async(UI) {
            val leagues = bg {
                service.getLeague().execute()
            }

            leagues.await().body()?.leagues?.let { this@TeamPresenter.view.spinerEntry(it) }
            view.hideLoading()
        }
    }
}