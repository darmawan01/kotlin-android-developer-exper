package com.darmawan.footballclubsubmission.views.team.detailteam

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.bumptech.glide.Glide
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.models.team.Team
import com.darmawan.footballclubsubmission.utils.ViewPagerAdapter
import com.darmawan.footballclubsubmission.views.team.detailteam.fragment.overview.OverViewFragment
import com.darmawan.footballclubsubmission.views.team.detailteam.fragment.player.PlayerFragment
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_detail_team.*

class DetailTeamActivity : AppCompatActivity() {

    private lateinit var presenter: DetailTeamPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)

        val team = intent.getParcelableExtra<Team>("TEAMS")
        Glide.with(this).load(team?.strTeamBadge).into(dtl_team_brand)
        dtl_team_name.text = team?.strTeam
        dtl_team_year.text = team?.intFormedYear
        dtl_team_manager.text = team?.strManager

        setFragment(team)

        toolbar_team.setOnClickListener {
            this.onBackPressed()
        }

        presenter = DetailTeamPresenter(this)
        presenter.getFavMatch(team.idTeam)

        if (isFavorite(team.idTeam)){
            btn_team_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_star_yellow_black_24dp))
        }

        btn_team_favorite.setOnClickListener {
            if (isFavorite(team.idTeam)){
                presenter.rmFavorite(team.idTeam)
                btn_team_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_star_black_24dp))
            }else{
                presenter.addFavorite(team)
                btn_team_favorite.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_star_yellow_black_24dp))
            }
        }

    }

    private fun setFragment(team: Team){

        val viewAdapter = ViewPagerAdapter(supportFragmentManager)

        val bundle = Bundle()
        bundle.putParcelable("TEAMS", team)

        val overview = OverViewFragment()
        overview.arguments = bundle

        val player = PlayerFragment()
        player.arguments = bundle

        viewAdapter.addFragment(overview, "Overview")
        viewAdapter.addFragment(player, "Player")

        dtl_viewpager.adapter = viewAdapter

        dtl_team_tab.setupWithViewPager(dtl_viewpager)
    }

    private fun isFavorite(idTeam: String?): Boolean {
        val item =  presenter.getFavMatch(idTeam)

        if (item.size > 0 ){
            return true
        }

        return false
    }
}
