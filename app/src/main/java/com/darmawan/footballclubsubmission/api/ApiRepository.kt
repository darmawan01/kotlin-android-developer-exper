package com.darmawan.footballclubsubmission.api

import com.darmawan.footballclubsubmission.models.brand.Brand
import com.darmawan.footballclubsubmission.models.brand.BrandResponse
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.models.event.EventResponse
import com.darmawan.footballclubsubmission.models.league.LeagueResponse
import com.darmawan.footballclubsubmission.models.player.PlayerResponse
import com.darmawan.footballclubsubmission.models.team.TeamResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiRepository {

    @GET("api/v1/json/1/eventspastleague.php")
    fun getPastEvent(@Query("id") id: String?): Call<EventResponse>

    @GET("api/v1/json/1/eventsnextleague.php")
    fun getNextEvent(@Query("id") id: String?): Call<EventResponse>

    @GET("api/v1/json/1/lookupevent.php")
    fun getEventDetail(@Query("id") id: String?): Call<EventResponse>

    @GET("api/v1/json/1/lookupteam.php")
    fun getTeamBrand(@Query("id") id: String?): Call<BrandResponse>

    @GET("api/v1/json/1/all_leagues.php")
    fun getLeague(): Call<LeagueResponse>

    @GET("api/v1/json/1/lookup_all_teams.php")
    fun getTeam(@Query("id") id: String?): Call<TeamResponse>

    @GET("api/v1/json/1/lookup_all_players.php")
    fun getPlayer(@Query("id") id: String?): Call<PlayerResponse>


}