package com.darmawan.footballclubsubmission.utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ViewPagerAdapter(fragment: FragmentManager): FragmentPagerAdapter(fragment){

    private val fragments = mutableListOf<Fragment>()
    private val titleFragment = mutableListOf<String>()

    override fun getItem(position: Int): Fragment = fragments.get(position)

    override fun getCount(): Int = fragments.size

    fun addFragment(fragment: Fragment, title: String){
        fragments.add(fragment)
        titleFragment.add(title)
    }
    override fun getPageTitle(position: Int): CharSequence? = titleFragment.get(position)
}