package com.darmawan.footballclubsubmission.utils

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*
import java.sql.Types.DATE

class DBopenHelper(context: Context): ManagedSQLiteOpenHelper(context, "match"){

    companion object {
        private var instance: DBopenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DBopenHelper {
            if (instance == null) {
                instance = DBopenHelper(ctx.applicationContext)
            }
            return instance as DBopenHelper
        }
    }
    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
                "FavMatch", true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "idEvent" to TEXT,
                "idSoccerXML" to TEXT,
                "idHomeTeam" to TEXT,
                "strHomeTeam" to TEXT,
                "intHomeScore" to TEXT,
                "idAwayTeam" to TEXT,
                "strAwayTeam" to TEXT,
                "intAwayScore" to TEXT,
                "dateEvent" to TEXT,
                "strDate" to TEXT
        )

        db?.createTable(
                "FavTeam", true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "idTeam" to TEXT,
                "strTeam" to TEXT,
                "strDescriptionEN" to TEXT,
                "strTeamBadge" to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable("FavMatch", true)
        db?.dropTable("FavTeam", true)
    }

}

val Context.database: DBopenHelper
    get() = DBopenHelper.getInstance(applicationContext)
