package com.darmawan.footballclubsubmission.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.models.player.Player
import com.darmawan.footballclubsubmission.models.team.Team
import org.jetbrains.anko.find

class AdapterPlayer(private val data: MutableList<Player>, private val listener: (Player)->Unit): RecyclerView.Adapter<AdapterPlayer.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.player_list, parent, false)
        return AdapterPlayer.ViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(data[position], listener)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        private val playerName: TextView = view.find(R.id.txt_player_name)
        private val playerLogo: ImageView = view.find(R.id.img_view_player)
        private val position: TextView = view.find(R.id.txt_player_position)

        fun bindItem(player: Player, listener: (Player) -> Unit){
            playerName.text = player.strPlayer
            position.text = player.strPosition
            Glide.with(itemView).load(player.strCutout).into(playerLogo)

            itemView.setOnClickListener {
                listener(player)
            }
        }
    }
}
