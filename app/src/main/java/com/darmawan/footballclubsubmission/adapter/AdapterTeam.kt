package com.darmawan.footballclubsubmission.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.models.team.Team
import org.jetbrains.anko.find

class AdapterTeam(private val data: MutableList<Team>, private val listener: (Team)->Unit): RecyclerView.Adapter<AdapterTeam.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.team_card_view, parent, false)
        return AdapterTeam.ViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(data[position], listener)
    }

    fun setFilterList(team: MutableList<Team>){
        data.clear()
        data.addAll(team)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        private val teamName: TextView = view.find(R.id.txt_team_name)
        private  val teamlogo: ImageView = view.find(R.id.img_view_team)

        fun bindItem(team: Team, listener: (Team) -> Unit){
            teamName.text = team.strTeam
            Glide.with(itemView).load(team.strTeamBadge).into(teamlogo)

            itemView.setOnClickListener {
                listener(team)
            }
        }
    }
}
