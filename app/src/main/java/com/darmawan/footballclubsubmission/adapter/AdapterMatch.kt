package com.darmawan.footballclubsubmission.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.darmawan.footballclubsubmission.R
import com.darmawan.footballclubsubmission.models.event.Event
import com.darmawan.footballclubsubmission.utils.dateParsers
import org.jetbrains.anko.find

class AdapterMatch(private val data: MutableList<Event>, private val listener: (Event) -> Unit): RecyclerView.Adapter<AdapterMatch.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.match_card_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(data[position], listener)
    }

    fun setFilterList(event: MutableList<Event>){
        data.clear()
        data.addAll(event)
        notifyDataSetChanged()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val homeTeam: TextView = view.find(R.id.txt_home_team)
        private val awayTeam: TextView = view.find(R.id.txt_away_team)
        private val tvDate: TextView = view.find(R.id.txt_event_date)
        private val homeScore: TextView = view.find(R.id.txt_home_score)
        private val awayScore: TextView = view.find(R.id.txt_away_score)

        fun bindItem(events: Event, listener: (Event) -> Unit){
            homeTeam.text = events.strHomeTeam
            awayTeam.text = events.strAwayTeam
            tvDate.text = dateParsers(events.dateEvent)
            homeScore.text = events.intHomeScore
            awayScore.text = events.intAwayScore

            itemView.setOnClickListener {
                listener(events)
            }
        }
    }

}

