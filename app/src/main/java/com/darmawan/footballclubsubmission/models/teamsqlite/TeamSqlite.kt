package com.darmawan.footballclubsubmission.models.teamsqlite

import android.os.Parcel
import android.os.Parcelable

data class TeamSqlite(
        var idTeam: String? = null,
        var strTeam: String? = null,
        var strDescriptionEN: String? = null,
        var strTeamBadge: String? = null
): Parcelable{
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(idTeam)
        dest?.writeString(strTeam)
        dest?.writeString(strDescriptionEN)
        dest?.writeString(strTeamBadge)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TeamSqlite> {
        override fun createFromParcel(parcel: Parcel): TeamSqlite {
            return TeamSqlite(parcel)
        }

        override fun newArray(size: Int): Array<TeamSqlite?> {
            return arrayOfNulls(size)
        }
    }

}