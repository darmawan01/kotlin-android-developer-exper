package com.darmawan.footballclubsubmission.models.league

data class League(
    var idLeague: String? = null,
    var strLeague: String? = null,
    var strSport: String? = null,
    var strLeagueAlternate: String? = null
){
    override fun toString(): String {
        return strLeague.toString()
    }
}