package com.darmawan.footballclubsubmission.models.player

data class PlayerResponse(
        val player: MutableList<Player>
)