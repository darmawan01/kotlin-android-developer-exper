package com.darmawan.footballclubsubmission.models.team

import android.os.Parcel
import android.os.Parcelable

data class Team(
        var idTeam: String? = null,
        var idSoccerXML: String? = null,
        var intLoved: String? = null,
        var strTeam: String? = null,
        var strTeamShort: String? = null,
        var strAlternate: String? = null,
        var intFormedYear: String? = null,
        var strSport: String? = null,
        var strLeague: String? = null,
        var idLeague: String? = null,
        var strManager: String? = null,
        var strStadium: String? = null,
        var strKeywords: String? = null,
        var strRSS: String? = null,
        var strStadiumThumb: String? = null,
        var strStadiumDescription: String? = null,
        var strStadiumLocation: String? = null,
        var intStadiumCapacity: String? = null,
        var strWebsite: String? = null,
        var strFacebook: String? = null,
        var strTwitter: String? = null,
        var strInstagram: String? = null,
        var strDescriptionEN: String? = null,
        var strDescriptionDE: String? = null,
        var strDescriptionIT: String? = null,
        var strGender: String? = null,
        var strCountry: String? = null,
        var strTeamBadge: String? = null,
        var strTeamJersey: String? = null,
        var strTeamLogo: String? = null,
        var strTeamFanart1: String? = null,
        var strTeamFanart2: String? = null,
        var strTeamFanart3: String? = null,
        var strTeamFanart4: String? = null,
        var strTeamBanner: String? = null,
        var strYoutube: String? = null,
        var strLocked: String? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()){
    }
    override fun writeToParcel(parcel: Parcel?, flags: Int) {
        parcel?.writeString(idTeam)
        parcel?.writeString(idSoccerXML)
        parcel?.writeString(intLoved)
        parcel?.writeString(strTeam)
        parcel?.writeString(strTeamShort)
        parcel?.writeString(strAlternate)
        parcel?.writeString(intFormedYear)
        parcel?.writeString(strSport)
        parcel?.writeString(strLeague)
        parcel?.writeString(idLeague)
        parcel?.writeString(strManager)
        parcel?.writeString(strStadium)
        parcel?.writeString(strKeywords)
        parcel?.writeString(strRSS)
        parcel?.writeString(strStadiumThumb)
        parcel?.writeString(strStadiumDescription)
        parcel?.writeString(strStadiumLocation)
        parcel?.writeString(intStadiumCapacity)
        parcel?.writeString(strWebsite)
        parcel?.writeString(strFacebook)
        parcel?.writeString(strTwitter)
        parcel?.writeString(strInstagram)
        parcel?.writeString(strDescriptionEN)
        parcel?.writeString(strDescriptionDE)
        parcel?.writeString(strDescriptionIT)
        parcel?.writeString(strGender)
        parcel?.writeString(strCountry)
        parcel?.writeString(strTeamBadge)
        parcel?.writeString(strTeamJersey)
        parcel?.writeString(strTeamLogo)
        parcel?.writeString(strTeamFanart1)
        parcel?.writeString(strTeamFanart2)
        parcel?.writeString(strTeamFanart3)
        parcel?.writeString(strTeamFanart4)
        parcel?.writeString(strTeamBanner)
        parcel?.writeString(strYoutube)
        parcel?.writeString(strLocked)

    }
    override fun describeContents(): Int {
        return 0
    }


    companion object CREATOR : Parcelable.Creator<Team> {
        override fun createFromParcel(parcel: Parcel): Team {
            return Team(parcel)
        }

        override fun newArray(size: Int): Array<Team?> {
            return arrayOfNulls(size)
        }
    }
}