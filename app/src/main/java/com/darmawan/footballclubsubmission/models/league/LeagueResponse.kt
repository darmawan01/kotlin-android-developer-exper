package com.darmawan.footballclubsubmission.models.league

data class LeagueResponse(
        val leagues: MutableList<League>
)