package com.darmawan.footballclubsubmission.models.brand

data class BrandResponse(
        val teams: List<Brand>
)