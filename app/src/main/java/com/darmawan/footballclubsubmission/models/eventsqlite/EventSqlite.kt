package com.darmawan.footballclubsubmission.models.eventsqlite

import java.util.*

data class EventSqlite(
        val idEvent: String? = null,
        val idSoccerXML: String? = null,
        val strHomeTeam: String? = null,
        val strAwayTeam: String? = null,
        val intHomeScore: String? = null,
        val intAwayScore: String? = null,
        val dateEvent: String? = null,
        val strDate: String? = null
)