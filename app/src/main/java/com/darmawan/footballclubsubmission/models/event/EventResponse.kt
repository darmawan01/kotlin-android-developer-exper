package com.darmawan.footballclubsubmission.models.event

import com.darmawan.footballclubsubmission.models.event.Event

data class EventResponse(
        val events: MutableList<Event>
)