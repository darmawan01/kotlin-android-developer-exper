package com.darmawan.footballclubsubmission.models.team

data class TeamResponse (
        val teams: MutableList<Team>
)