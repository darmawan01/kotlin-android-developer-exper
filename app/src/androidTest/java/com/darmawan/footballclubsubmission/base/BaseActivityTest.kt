package com.darmawan.footballclubsubmission.base

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import org.junit.Rule
import com.darmawan.footballclubsubmission.R.id.*
import com.darmawan.footballclubsubmission.views.base.BaseActivity
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BaseActivityTest{

    @Rule
    @JvmField var activityTest = ActivityTestRule(BaseActivity::class.java)

    @Test
    fun testAppBehaviour(){

        Thread.sleep(2000)
        onView(withId(rc_prev)).check(matches(isDisplayed()))

        onView(withId(rc_prev)).perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Thread.sleep(3000)
        onView(withId(txt_dtl_date_event)).check(matches(isDisplayed()))
        onView(withId(txt_dtl_away_defense)).check(matches(isDisplayed()))

        Thread.sleep(2000)
        onView(withId(btn_favorite))
                .check(matches(isDisplayed()))
        onView(withId(btn_favorite)).perform(click())

        Thread.sleep(2000)
        onView(withId(toolbar))
                .check(matches(isDisplayed()))
        onView(withId(toolbar)).perform(click())

        Thread.sleep(3000)
        onView(withId(navigation_next_match))
                .check(matches(isDisplayed()))
                .perform(click())

        Thread.sleep(3000)
        onView(withId(rc_next)).check(matches(isDisplayed()))

        Thread.sleep(3000)
        onView(withId(navigation_favorite))
                .check(matches(isDisplayed()))
                .perform(click())

        Thread.sleep(3000)
        onView(withId(rc_favorite)).check(matches(isDisplayed()))

        Thread.sleep(2000)
        onView(withId(rc_favorite)).perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        Thread.sleep(3000)
        onView(withId(txt_dtl_away_team_logo)).check(matches(isDisplayed()))
        onView(withId(txt_dtl_home_team_logo)).check(matches(isDisplayed()))

    }

}